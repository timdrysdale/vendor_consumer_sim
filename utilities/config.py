#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""config.py
   
   utilities for handling config files

   automatically assigns port names
   names and ports are passed to each entity, as a directory
   the intent is that names can be kept consistent across runs
   if need be, e.g. so that an agent can contact a favourite 
   vendor/consumer, that it has learned about in a previous
   simulation (even if its port number changes from run to run,
   it can be found by looking up its name in the directory).
   
"""

import json
import logging #need the constants for logging level
from ..utilities import logger

def reader(configfile):

    log = logger.get_logger(__name__, logging_level = logging.WARNING)
    
    log.info("Opening config file {}".format(configfile))
    
    """ assume a JSON format file... """
    with open(configfile) as json_file:  
        config = json.load(json_file)

    """    
    Assign sockets into configs based on names
    use dictionaries, pass dictionaries to entities
    entities can update the dict with the socket object
    will require dict search for type, then name, or random choice

    Assign sockets incremently starting at given port
    This should be sufficient that a user can avoid ports in use
    Example config:
    "sockets":{
     "url":"tcp://127.0.0.1:",
     "port":{"start":5555}
    }
    """
    url = config['sockets']['url']
    start_port = config['sockets']['port']['start']
    ports_used = 0

    
    """ Build dictionaries of agents, their sockets, configs, and lists of their contacts 
    world = { "details_of":{
      "vendor0":{
          "is":"vendor",
          "socket":"tcp://127.0.0.1:5555"
       }, ...
      },
      "config_of":{
       "vendor0":{<config details>},

      },
      "list_of":{
         "vendors":["vendor0"],
         "consumers":[], ...
     }
    }

    """
    
    world = {"details_of":{},
             "config_of":{},
             "list_of":{
                 "clocks":[],
                 "consumers":[],
                 "vendors":[],
                 "vendorconsumers":[]
             }
    }

    agents = [
        { "type":"consumers",
          "configs": config['consumers']
        },
        { "type":"clocks",
          "configs": config['clocks']
        },
        { "type":"vendors",
          "configs": config['vendors']
        },
        { "type":"vendorconsumers",
          "configs": config['vendorconsumers']
        }
    ]

    
    if 'heartbeat' in config:
        world['details_of']['heartbeat'] = config['heartbeat']
    else:
        world['details_of']['heartbeat'] = { "beater_delay_seconds": 0.1,
                                             "monitor_period_seconds": 1}
        
  

    for agent_class in agents:
        agent_type = agent_class['type']
        for agent in agent_class['configs']:
            agent_name = agent['name']
            log.debug("Configuring {} {}".format(agent_type, agent_name))

            #Assign a socket
            #note that socket addresses must be bytes for nanomsg, hence encode('utf-8')
            socket = "{}{}".format(url, start_port + ports_used).encode('utf-8')
            ports_used = ports_used + 1
            
            #update the agent's config
            agent['config']['name'] = agent_name
            agent['config']['my_socket'] = socket
            agent['config']['my_type'] = agent_type

            #add agent details and config to the world and include in directory lists
            world['details_of'][agent_name] = {'is': agent_type,
                                                'url': socket}

            if agent_type == u'clocks':
                #Assign a second socket for turn synchronisation messages with agents
                sync_socket = "{}{}".format(url, start_port + ports_used).encode('utf-8')
                ports_used = ports_used + 1
                agent['config']['sync_socket'] = sync_socket
                world['details_of'][agent_name]['url_sync'] = sync_socket
            
            #update the agent's config
            agent['config']['name'] = agent_name
            agent['config']['my_socket'] = socket
            
            #advertise the list of topics on offer, if agent is a publisher
            if 'list_of_topics' in agent['config']:
                world['details_of'][agent_name]['list_of_topics'] = agent['config']['list_of_topics']
            
            world['config_of'][agent_name] = agent['config']

            world['list_of'][agent_type].append(agent_name)

    #update every entities config with the directory they need for connecting
    for agent_name, agent_config in world['config_of'].items():
        agent_config['others']={}
        agent_config['others']['list_of'] = world['list_of']
        agent_config['others']['details_of'] = world['details_of']

    for agent_name, agent_config in world['config_of'].items():
        log.debug("{} has config {}".format(agent_name, agent_config))


        
    
    return world


