#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""

Suggested message formats, and defined words to help avoid silent errors 
from spelling mistakes in strings (mispelt variable names cause an error)

{ CATEGORY: <category>,
  ACTION: <action>,
  DETAILS:<data>,
}

CATEGORY can be COMMAND, ALERT, SIM etc

ACTION depends on the category, for example: 

ALERT: [FINISHED_TURN, REGISTERED_FOR_CLOCK, WARNING]
COMMAND: [BEGIN_TURN, END_TURN]
SIM: [ACCEPT_OFFER, DECLINE_OFFER, NEGOTIATE_OFFER, OFFER_TO_BUY, OFFER_TO_SELL, REQUEST_CATALOGUE, REQUEST_NEEDS]

Note that ALERT, and COMMAND are intended for use by the underlying simulation engine, 
while policies should usually use the SIM category.

DETAILS is for a dict with further information in key:values. Some messages have mandatory details fields that 
must be included, while others can accept optional details.

Message handlers should ignore additional fields they don't understand, but are encouraged to flag bad messages by replying 
with a WARNING or dropping a log.warning or log.critical so as to aid in debugging (silent message dropping is harder to troubleshoot)

"""

import json

# Fields
CATEGORY = u'category'
ACTION = u'action'
DETAILS = u'details'

# Categories
ALERT = u'alert'
COMMAND = u'command'
SIM = u'sim'

#Actions for ALERT
ACKNOWLEDGED = u'acknowledged'
FINISHED_TURN = u'finished_turn'
REGISTERED_FOR_CLOCK = u'registered_for_clock'
WARNING = u'warning'

#Actions for COMMAND
BEGIN_TURN = u'begin_turn'
END_TURN = u'end_turn'
SEND_TO = u'send_to'
SEND = u'send'

#Actions for SIM - feel free to EXTEND with new actions but do NOT MODIFY existing else old policies may break
ACCEPT_OFFER = u'accept_offer'
DECLINE_OFFER = u'decline_offer'
NEGOTIATE_OFFER = u'negotiate_offer'
OFFER_TO_BUY = u'offer_to_buy'
OFFER_TO_SELL = u'offer_to_sell'
REQUEST_CATALOGUE = u'request_catalogue'  #e.g. catalogue lists products, prices and other details
REQUEST_NEEDS = u'request_needs'

#Details - feel free to EXTEND with new details, but do NOT DELETE existing else sim code may break
ACTIVE_INTERUPTED = u'active_interupted'
AGENT_FINISHED_TURN = u'agent_finished_turn'
ALL_REGISTERED_FOR_CLOCK = u'all_registered_for_clock'
ALL_AGENTS_FINISHED_TURN = u'all_agents_finished_turn'
AVAILABLE = u'available'
CONTENT = u'content'
COMMENT = u'comment'
CONSUMER = u'consumer'
CONTACT = u'contact'
CORE = u'core'
DESCRIPTION = u'description'
DONE = u'done'
ERROR = u'error'
EXPECTED = u'expected'
EXTRAS = u'extras'
FINISHED_COUNT = u'finished_count'
JSONDecodeError = u'JSONDecodeError'
LOG_SALE = u'log_sale'
MAX = u'max'
MESSAGE = u'message'
MIN = u'min'
MOQ = u'moq' #minimum order quantity
NAME = u'name'
NUMBER = u'number'
OFFER = u'offer'
PASSIVE_INTERUPTED = u'passive_interupted'
POLICY_TASK_ENDED = u'policy_task_ended'
PRODUCT = u'product'
PRICE = u'price'
PUBLISHING = u'publishing'
QUANTITY = u'quantity'
RECEIVED = u'received'
REGISTERED = u'registered'
SOLD = u'sold'
STATE_INTERUPTED = u'state_interupted'
SOCKETS_CLOSED = u'sockets_closed'
SOURCE = u'source'
SUBSCRIBER_ADDED = u'subscriber_added'

TO = u'to'
TOTAL_PRICE = u'total_price'
TURN = u'turn'
TYPE = u'type'
UNEXPECTED_MESSAGE = u'unexpected_message'
UNIT_PRICE = u'unit_price'
VENDOR = u'vendor'

# Message construction helpers for policies

def send_message(message, to = ''):
    """ for policy to request via a queue to send message
    the default is to not specify a destination, but for 
    the case of REQ sockets, put a name in the kwarg 'to'
    e.g. msg.send_message(msg.offer_to_sell(msg.offer_details(<details>)), to=u'consumer0')
    Note the message is expected to be a JSON dict with its own CATEGORY, ACTION and EXTRAS
    """
    if to == '':
        return { CATEGORY: COMMAND,
                 ACTION: SEND,
                 EXTRAS: {
                     MESSAGE: message
                 }}
    else:
        return { CATEGORY: COMMAND,
                 ACTION: SEND_TO,
                 EXTRAS: {
                     TO: to,
                     MESSAGE: message
                 }}


def offer_details(product, price, vendor, consumer, quantity = 1, extras = {}):
    """ for consumer or vendor to construct an offer """
    return{ PRODUCT: product,
            PRICE: price,
            QUANTITY: quantity,
            VENDOR: vendor,
            CONSUMER: consumer,
            EXTRAS: extras
    }

def range_quantity(min, max):
    """ some offers need a range of quantities """
    return {MIN: min, MAX: max}

def offer_to_sell(offer):
    return { CATEGORY: SIM,
             ACTION : OFFER_TO_SELL,
             OFFER: offer}
    
def offer_to_buy(offer):
    return { CATEGORY: SIM,
             ACTION : OFFER_TO_BUY,
             OFFER: offer}

def accept_offer(offer, quantity, extras = {}): 
    """ in a simple model, both sides will assume delivery so this completes transaction """
    return {  CATEGORY: SIM,
              ACTION: ACCEPT_OFFER,
              OFFER: offer,
              QUANTITY: quantity,
              EXTRAS: extras }

def decline_offer(offer, extras = {}):
    """ product/price/extras are probably redundant ... """
    return { CATEGORY: SIM,
             ACTION: DECLINE_OFFER,
             OFFER: offer,
             EXTRAS: extras }

# Message identification / extraction / conversion helpers for policy code

def get_offer(message):
    try:
        return message[OFFER]
    except KeyError:
        return {}
    
def is_offer(message):
    try:
        return (message[CATEGORY] == SIM and message[ACTION] in [OFFER_TO_SELL, OFFER_TO_BUY, OFFER_TO_NEGOTIATE])
    except KeyError:
        return False

def is_accepting_offer(message):
    try:
        return (message[CATEGORY] == SIM and message[ACTION] == ACCEPT_OFFER)
    except KeyError:
        return False 

def get_quantity(message):
    try:
        quantity = message[QUANTITY]
        
        if isinstance(quantity, int):
            return quantity
        else:
            raise KeyError #it's not quite a keyError but we handle it the same
        
    except KeyError:
        log.critical(msg.simlog("Offer acceptance requires an integer quantity msg.QUANTITY:<int>", extras = {msg.MESSAGE: message}))
        return 0
    
def get_product(message):
    try:
        return message[OFFER][PRODUCT]
    
    except KeyError: #it's not quite a keyError but we handle it the same
        return None 
    
def log_sale(me, contact, reply):
    # note that in dict merge second dict overwrites first if conflict
    try:
        return {
            CATEGORY: SIM,
            ACTION: LOG_SALE,
            VENDOR: me,
            CONSUMER: contact,
            PRODUCT: reply[OFFER][PRODUCT],
            QUANTITY: reply[QUANTITY],
            UNIT_PRICE: reply[OFFER][PRICE],
            TOTAL_PRICE: reply[OFFER][PRICE] * reply[QUANTITY],
            EXTRAS: {**reply[OFFER][EXTRAS], **reply[EXTRAS]} 
        }
    except KeyError:
        log.critical(log.simlog("can't log sale properly", extras = {msg.MESSAGE, reply}))
        return {
            CATEGORY: SIM,
            ACTION: LOG_SALE,
            REPLY: reply
        }
    
# Message construction helpers for agent core code

def begin_turn(turn):
    return { CATEGORY: COMMAND,
             ACTION: BEGIN_TURN,
             EXTRAS: {TURN: turn}}

def finished_turn(name, turn):
    return { CATEGORY: ALERT,
             ACTION: FINISHED_TURN,
             EXTRAS: {TURN: turn, NAME: name}}
    
def registered_for_clock(name):
    return { CATEGORY: ALERT,
             ACTION: REGISTERED_FOR_CLOCK,
             EXTRAS: {NAME: name}}

def acknowledge(message):
    """ REQREP pattern requires a REP for every REQ so use acknowledge as a dummy message where required """
    return { CATEGORY: ALERT,
             ACTION: ACKNOWLEDGED,
             EXTRAS: {MESSAGE: message}}

# Message identification helpers for agent core code

def recipient(message):
    return message[EXTRAS][TO]
def content(message):
    return message[EXTRAS][MESSAGE]

def is_send_to_command(message):
    try:
        return (message[CATEGORY] == COMMAND and message[ACTION] == SEND_TO and TO in message[EXTRAS])
    except KeyError:
        return False   

def is_send_command(message):
    try:
        return (message[CATEGORY] == COMMAND and message[ACTION] == SEND)
    except KeyError:
        return False
    
def is_begin_turn_command(message):
    try:
        return (message[CATEGORY] == COMMAND and message[ACTION] == BEGIN_TURN and TURN in message[EXTRAS])
    except KeyError:
        return False

def turn_number(message):
    return message[EXTRAS][TURN]

def is_alert_that_finished_turn(message):
    try:
        return (message[CATEGORY] == ALERT and message[ACTION] == FINISHED_TURN)
    except KeyError:
        return False


def is_alert_that_registered_for_clock(message):
    try:
        return (message[CATEGORY] == ALERT and message[ACTION] == REGISTERED_FOR_CLOCK)
    except KeyError:
        return False
    
def log(comment, extras = {}, source = ''):
    """ make a message format that will print ok in the log file 
        If you don't use this, you can easily have JSON decode errors in processing the log file
        because the log's file handler is expecting conformant JSON and will put in ' instead of "
        or leave off "" entirely if you just put in a string etc ...
    """
    
    message = {COMMENT: comment}
    
    if source:
        message[SOURCE] = source
    if extras:
        message[EXTRAS] = extras
    return json.dumps(message)

def simlog(comment, extras = {}):
    return log(comment, extras, source = SIM)

def corelog(comment, extras = {}):
    return log(comment, extras, source = CORE)

def safe(suspect_thing):
    """ put a suspect thing into a string for logging, and show it's type 
        note that python types are not serialisable, so we format and print it """
    return {TYPE: "{}".format(type(suspect_thing)),
            CONTENT: "{}".format(suspect_thing)}
