#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""spawn.py
   
   spawning routines for agents

   Note when building 'contact' lists for each agent:

    * all non-clock agents subscribe to all clocks, 
          which all publish on topic 'time'

    * all non-clock, non-journal agents push to all 
          journals, which pull

    * all vendors offer a REQ-REP server, to which all 
          consumers and vendorconsumers set up a socket to REQ

    * all consumers offer a REQ-REP server, to which all 
          vendors and vendorconsumers set up a socket to REQ

    * all vendorconsumers offer a REQ-REP server, to which
            all consumers, vendors and vendorconsumers set up a socket to REQ
            (note that vendorconsumers should not REQREP themselves) 
 
   
    We're working from a config with this format:
    
    config = { "details_of":{
      "vendor0":{
          "is":"vendor",
          "at":"tcp://127.0.0.1:5555"
       }, ...
      },
      "config_of":{
       "vendor0":{<config details>},

      },
      "list_of":{
         "vendors":["vendor0"],
         "consumers":[], ...
     }
    }


"""

import asyncio
from ..agents import agent, clock
import logging
from ..utilities import heartbeat, logger, msg, wrap

agent_implementation = {
    "clocks": clock,
    "consumers": agent,
    "vendors": agent,
    "vendorconsumers": agent
}

def agents_of_type(agent_type, config, queue, stop_event):
    log = logger.get_logger(__name__, logging_level = logging.INFO)

    tasks = set()
    
    if not (agent_type == u'vendorconsumers'): #instead add one per vendorconsumer in run.py
          tasks.add(heartbeat.beater(config, queue, agent_type, stop_event))
    
    for agent_name in config['list_of'][agent_type]:

        agent_config = config['config_of'][agent_name]

        agent_config['others'] = {
            "details_of": config['details_of'],
            "list_of": config['list_of']
        }

        agent_config['stop_event'] = stop_event
        
        #assign a coro to run this agent
        task = agent_implementation[agent_type].get_coro(agent_config['coro'])(agent_config)
        tasks.add(task)
        log.debug(msg.corelog("Created agent", extras = {msg.NAME: agent_name}))
        
        
    return tasks
    

