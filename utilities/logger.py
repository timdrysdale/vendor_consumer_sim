#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""logger.py
   
   Modified from https://www.toptal.com/python/in-depth-python-logging 
   
"""

import logging
import sys

#FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
FORMATTER = logging.Formatter('{"time":"%(asctime)s","name":"%(name)s","level":"%(levelname)s","content":%(message)s}')

DEFAULT_LOG_FILE = "vcs.log"

def get_console_handler():
   console_handler = logging.StreamHandler(sys.stdout)
   console_handler.setFormatter(FORMATTER)
   return console_handler

def get_file_handler(filename = DEFAULT_LOG_FILE):
   file_handler = logging.FileHandler(filename)
   file_handler.setFormatter(FORMATTER)
   return file_handler

def get_logger(logger_name, logging_level = logging.INFO, to_file = True, filename = DEFAULT_LOG_FILE):
   logger = logging.getLogger(logger_name)
   logger.setLevel(logging_level)
   if (logger.hasHandlers()):
      logger.handlers.clear() #avoid double reporting
   logger.addHandler(get_console_handler())

   if to_file:  #log to file as well...
       logger.addHandler(get_file_handler(filename))
       
   # with this pattern, it's rarely necessary to propagate the error up to parent
   logger.propagate = False
   return logger
