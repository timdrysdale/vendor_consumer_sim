#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" heartbeat.py
   
   implements heartbeats that will alert if a process blocks 
   Intended to be useful during development! 

   assembles a list of heartbeats, then checks that 
   any heartbet it has ever got is still coming
   times out after 1 second of missing a heartbeat

"""
import asyncio
import logging
import multiprocessing
import queue
import time
from ..utilities import logger, msg

async def check(beats, max_time, stop):

    log = logger.get_logger(__name__ + '_check', logging_level = logging.INFO)
  
    log.debug(msg.corelog("Heartbeat monitor starting checker service"))

    missed = {}
    
    while stop.is_set() == False:
        await asyncio.sleep(1)
        
        for name, last_beat in beats.items():
            age = time.time() - last_beat
            log.debug("{} {}".format(name, age))
            if age > max_time:
                if name in missed:
                    missed[name] = missed[name] + 1
                else:
                    missed[name] = 1
                log.warning(msg.corelog("Missing heartbeats from {}/{} processes [{}]".format(len(missed),len(beats), missed)))

                for name, count in missed.items():
                    if count > 5:
                        log.critical(msg.corelog("Heartbeats missing for last five checks from {}".format(name)))
                        log.critical(msg.corelog("Check for blocking commands in your policy module"))
                        stop.set()
                       

    return

async def monitor(config, queue, stop):

    log = logger.get_logger(__name__ +'_monitor', logging_level = logging.INFO)

    log.debug(msg.corelog("Starting heartbeat monitor"))
    
    max_time = config['details_of']['heartbeat']['monitor_window_seconds']

    beats = {}

    check_task = asyncio.create_task(check(beats, max_time, stop))                            

    while stop.is_set() == False:
        await asyncio.sleep(0)
        try:
            message = queue.get(timeout = 0.1)
        except multiprocessing.queues.Empty:
            pass
        else:
            
            #update beats dict with latest time
            beat_name = message #.decode('utf-8')
            if not (beat_name in beats):
                log.info(msg.corelog("Monitoring heartbeats from {}".format(beat_name)))
            
            beats[message] = time.time()
            log.debug(msg.corelog("Heartbeat monitor received heartbeat from {}".format(beat_name)))

    await check_task
    
    return                           
                               
                               
async def beater(config, queue, name, stop):
    
    log = logger.get_logger(__name__, logging_level = logging.INFO)
    
    log.debug(msg.corelog("Starting heartbeater for {}".format(name)))

    beater_sleep_seconds = config['details_of']['heartbeat']['beater_delay_seconds']

    while stop.is_set() == False:
        await asyncio.sleep(beater_sleep_seconds)
        queue.put(name)
        log.debug(msg.corelog("Heartbeater sending heartbeat for {}".format(name)))

    return
