# vendor_consumer_sim

## Background

This was created by Tim Drysdale to support an MSc student to investigate the interactions of automated vendors and consumers that follow coded policies. The motivation behind the project was to explore scenarios that relate to Universities sharing undergraduate teaching equipment (i.e. remote laboratories) so as to increase the diversity of equipment, the throughput and the resilience. In this sense, each vendor is also a consumer, and therefore there are expected to be interesting dynamics and it would be useful to understand whether there are behaviours, principles or policies that should be encouraged or discouraged according to what works for the greater good of a network of academics and their students who wish to pool their remote lab resources.

## VCS

In its current form, VCS is a work in progress, and represents only the simplest of V-C interactions, akin to simple robotic systems interaction in online shops. Future versions are hoped to include VC robots that both vend and consume. Meanwhile ...


## Agent interconnection rules


    * all non-clock agents subscribe to all clocks, 
          which all publish on topic 'time'

    * all vendors offer a REQ-REP server, to which all 
          consumers and vendorconsumers set up a socket to REQ

    * all consumers offer a REQ-REP server, to which all 
          vendors and vendorconsumers set up a socket to REQ

    * all vendorconsumers offer a REQ-REP server, to which
            all consumers, vendors and vendorconsumers set up a socket to REQ
            (note that vendorconsumers should not REQREP themselves)     


## Usage

clone the module repository to your disk, then make a simlink in the site-packages directory for your python3 implementation

```ln -s /home/tim/Documents/code/vendor_consumer_sim /home/tim/anaconda3/lib/python3.7/site-packages/vcs```

This allows the module to be referenced by a shorter name (the name of the symlink in sitepackages is here ```vcs```). Then the module can be run from any other directory, thus keeping config files and log files organised, and separate from development code.



```python -m vcs.run --config=myconfig.json 2> vcs.err```

A config file is used to establish a scenario with a number of vendors and consumers, a clock and a logger. There can be multiple vendors, multiple consumers, but usually only one clock, and usually only one journal. A config file is mandatory.

A quick summary report can be obtained afterwards using:

```python -m vcs.reports.summary --log=vcs.log ```

### Clock

The clock coordinates the simulation by keeping orderly progress through a series of turns, which it announces. The clock initially waits for all the expected agents to register that they have subscribed to the clock publish socket, then the first turn is announced. Once all the agents have completed their turn, they alert the clock. When all agents have alerted the clock that their turn is finished, the clock waits briefly to avoid race conditions occurring between turns, then announces the new turn number.

### Logging

All agents are capable of logging to the log file ```vcs.log``` which is put in the working directory (where you started the module from). Even though the agents can be split across multiple processes, they all have access (and the logger is multiprocessing aware so that messages do not collide with each other).

Each line is a JSON object, to facilitate post-processing. A simple demonstration can be found in ```./reports/summary.py```

### Agents

Agents are classified into vendors, consumers, and vendorconsumers. The same code is used to run each agent - the distinction simply allows the contact list to be customised for each type of agent. E.g. vendors only want to make offers to consumers and vendorconsumers, not other vendors. Each agent sets up a REQ socket to bind to the REP socket of every agent in its contact list. The assignment, and discovery, of port numbers for the sockets is handled automatically.

### Policies

Policies are plug-ins that drive the simulation from the point of view of modelling pricing, selling and buying behaviour. An example ```./examples/basic.json``` has a single vendor selling oranges at a fixed price to three consumers and vendorconsumer that have different price thresholds, and a random buying probability.

It is relatively easy to create logic that "hangs" so it is recommended that you develop a test-harness for your more complicated policies so that they can be tested outwith the simulation. Debugging within the simulation requires careful inspection of the error file ```vcs.err``` and the log file ```vcs.log``` using grep.

For example, an initial look for Errors

```cat vcs.err | grep Error```

Let's say you can see a KeyError and some asyncio.CancelledErrors. Ignore the cancelled errors - they came from you hitting Ctrl-C to stop a hung simulation. Show more lines before the KeyError to see which file it was in:
```cat vcs.err | grep KeyError -B 10```

If there are no obvious errors in the error file, then a logic error is hanging the policy. You can find the offending agent by inspecting the reports to the clock of who has finished their turn:

```cat vcs.log | grep finished_count```

You can look for offers:

```cat vcs.log | grep offers```

If you need to, you can edit the logging level in the policy code, or ```./agent/agent.py```, ```./clock/clock.py```


### Multiprocessing

Unfortunately pyzmq issuie #1208 indicates an issue with having a send and recv awaiting on the same socket in the same event loop. This has been verified by simple example. Consequently, this rules out having the sender and receiver of a message in the same process. Multiprocessing has been used to run groups of agents in separate processes, i.e. vendors in one grup, and consumers in another, seeing as vendors don't talk to other vendors. Vendorconsumers might talk to other vendorconsumers, so each has its own process. The processes are not particularly CPU heavy.

### Heartbeat
There is a heartbeat utility running in each process, that will detect you creating an event loop blockage, which it will flag with a critical error message. You usually won't need to see the individual heartbeats being reported, but if you need reassurance, lower the logging level in the coroutines in ```./utilities/heartbeat```.

## Config file format
Each agent is configured in a JSON file whose named is passed to the run function at the start of a simulation. Editing JSON can be tricky if you get the punctuation wrong - using a JSON formatter or validator is helpful with this.

Example config file:
```
{
    "name":"basic.json",
    "description":"basic test configuration",
    "author":"TDD",
    "date":"2019-02-19",
    "vendorconsumers":[
	{"name":"vendorconsumer0",
	 "config":{
	     "coro":"basic",
	     "policy":{
		 "class":"demo",
		 "coro":"markup",
		 "parameters":{
		     "cash":5.00,
		     "desires":[
			 {"category":"fruit"},
			 "oranges"],
		     "desires":"oranges",
		     "price":{
			 "max":0.55
		     },
		     "probability":0.3,
		     "quantity":{
			 "min":1,
			 "max":3
		     }
		     }
		 }
	     }
	 }
	
    ],
    "vendors":[
	{"name":"vendor0",
	 "config":{
	     "coro":"basic",
	     "policy":{
		 "class":"demo",
		 "coro":"sell_bulk",
		 "parameters":{
		     "products": [
			 {"price":0.21,
			  "moq": 3,
			  "name":"oranges",
			  "description":"naval",
			  "category": "fruit",
			  "quantity": 24
			 
			 }
			 
		     ]
			 
		 }
	     }
	 }
	}
    ],
    "consumers":[
	{"name":"buyer0",
	 "config":{
	     "coro":"basic",
	     "policy":{
		 "class":"demo",
		 "coro":"buy_below_price",
		 "parameters":{
		     "desires":"oranges",
		     "price":{
			 "max":0.55
		     },
		     "probability":0.3,
		     "quantity":{
			 "min":1,
			 "max":3
		     }
		 }
		 
	     }
	 }
	},
	{"name":"consumer1",
	 "config":{
	     "coro":"basic",
	     "policy":{
		 "class":"demo",
		 "coro":"buy_below_price",
		 "parameters":{"desires":"oranges",
		     "price":{
			 "max":0.47
		     },
			       "probability":0.8,
		     "quantity":{
			 "min":3,
			 "max":6
		     }}
	     }
	 }
	},
		{"name":"consumer2",
	 "config":{
	     "coro":"basic",
	     "policy":{
		 "class":"demo",
		 "coro":"buy_below_price",
		 "parameters":{"desires":"oranges",
		     "price":{
			 "max":0.33
		     },
			       "probability":0.3,
		     "quantity":{
			 "min":6,
			 "max":18
		     }}
	     }
	 }
		}
    ],
    "clocks":[
	{"name":"clock",
	 "config":{
	     "coro":"basic",
	     "lifetime_seconds":1,
	     "list_of_topics":["time"],
	     "turns":100
	 }
	}
    ],
    "sockets":{
	"url":"tcp://127.0.0.1:",
	"port":{"start":5555}
    },
    "heartbeat":{
	"beater_delay_seconds": 0.5,
        "monitor_window_seconds": 3
    }
}

```


## Module structure

The simulations are started from ```run.py```, using ```./utilities``` to read the config file (```config.py```), spawn agent tasks (```spawn.py```), set up heartbeating (```heartbeat.py```), and standardise messaging (```msg.py```) and logging (```logger.py```). Agent logic is defined in ```./agent/agent.py``` with flags to adjust the definition of the config to suit the particular role it is playing (consumer, vendor, vendorconsumer). Each agent uses the base policy (```./policies/base.py```) in conjunction with a policy that is defined in a separate file, following the example of ```demo.py```. You are encouraged to create new files in ```./policies``` to hold your customised policies. An example configuration file is found in ```./examples/basic.json```. Please contribute example files to suit your new policies. 

```
.
├── agents
│   ├── agent.py
│   └── clock.py
├── examples
│   └── basic.json
├── LICENSE
├── policies
│   ├── base.py
│   └── demo.py
├── README.md
├── reports
│   └── summary.py
├── requirements.txt
├── run.py
└── utilities
    ├── config.py
    ├── heartbeat.py
    ├── logger.py
    ├── msg.py
    └── spawn.py
```

The code is currently sitting at just over 1000 lines:

```
github.com/AlDanial/cloc v 1.70  T=0.09 s (167.4 files/s, 26218.0 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          12            498            341           1192
JSON                             1              6              0            130
Markdown                         1             59              0            121
Bourne Shell                     1              1              0              1
-------------------------------------------------------------------------------
SUM:                            15            564            341           1444
-------------------------------------------------------------------------------

```

## Example output

Here's an example output from the example configuration file, after processing by ```./report/summary``` (see usage).
it is expected that you will develop much more sophisticated post-processing and visualisation than this, particularly as
the complexity increases. However, the basic approach to post-processing the `one JSON object per line' log file demonstrated successfully.

```
Turn   5: vendor0 ->          buyer0 £0.63 for 3 oranges [●●●]
Turn  11: vendor0 ->       consumer1 £0.63 for 3 oranges [●●●]
Turn  20: vendor0 ->       consumer2 £1.68 for 8 oranges [●●●●●●●●]
Turn  29: vendor0 ->          buyer0 £0.63 for 3 oranges [●●●]
Turn  47: vendor0 ->       consumer1 £1.26 for 6 oranges [●●●●●●]
Turn  14: vendor0 ->       consumer1 £0.84 for 4 oranges [●●●●]
Turn  26: vendor0 ->       consumer1 £0.84 for 4 oranges [●●●●]
Turn  32: vendor0 -> vendorconsumer0 £0.63 for 3 oranges [●●●]
Turn  44: vendor0 ->          buyer0 £0.63 for 3 oranges [●●●]
Turn  47: vendor0 ->       consumer1 £1.05 for 5 oranges [●●●●●]
Turn  50: vendor0 -> vendorconsumer0 £0.63 for 3 oranges [●●●]
```

For a single command line, you can try:

```rm vcs.log && python -m vcs.run --config=basic.json | grep log_sale && python -m vcs.reports.summary --log=vcs.log```

Example output is

```
{"time":"2019-02-27 01:21:05,907","name":"vendor0","level":"INFO","content":{"comment": "sold", "source": "sim", "extras": {"category": "sim", "action": "log_sale", "vendor": "vendor0", "consumer": "buyer0", "product": "oranges", "quantity": 3, "unit_price": 0.21, "total_price": 0.63, "extras": {}}}}
{"time":"2019-02-27 01:21:06,756","name":"vendor0","level":"INFO","content":{"comment": "sold", "source": "sim", "extras": {"category": "sim", "action": "log_sale", "vendor": "vendor0", "consumer": "consumer1", "product": "oranges", "quantity": 3, "unit_price": 0.21, "total_price": 0.63, "extras": {}}}}
{"time":"2019-02-27 01:21:07,596","name":"vendor0","level":"INFO","content":{"comment": "sold", "source": "sim", "extras": {"category": "sim", "action": "log_sale", "vendor": "vendor0", "consumer": "vendorconsumer0", "product": "oranges", "quantity": 3, "unit_price": 0.21, "total_price": 0.63, "extras": {}}}}
{"time":"2019-02-27 01:21:08,411","name":"vendor0","level":"INFO","content":{"comment": "sold", "source": "sim", "extras": {"category": "sim", "action": "log_sale", "vendor": "vendor0", "consumer": "consumer2", "product": "oranges", "quantity": 13, "unit_price": 0.21, "total_price": 2.73, "extras": {}}}}
Turn   2: vendor0 ->          buyer0 £0.63 for 3 oranges [●●●]
Turn   8: vendor0 ->       consumer1 £0.63 for 3 oranges [●●●]
Turn  14: vendor0 -> vendorconsumer0 £0.63 for 3 oranges [●●●]
Turn  20: vendor0 ->       consumer2 £2.73 for 13 oranges [●●●●●●●●●●●●●]
```

## TODO

Testing has been woefully inadequate - there are no formal tests instantiated. Please consider developing tests and putting them in ```./tests```.




