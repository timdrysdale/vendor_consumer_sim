#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""summary.py
   
   summarises the offers and sales made for basic agents using the sell_bulk and buy_below_price policies

   
"""
import argparse
import json
import time
from ..utilities import  msg


def is_begin_turn(obj):
    try:
        return obj['name'] == "vcs.agents.clock" and obj['content']['comment']== msg.BEGIN_TURN
    except:
        return False

def get_turn(obj):
    try:
        if is_begin_turn(obj):
            return obj['content'][msg.EXTRAS][msg.TURN]
    except KeyError:
        return None

def is_sale(obj):
    """
    {"time":"2019-02-26 23:49:08,282","name":"vendor0","level":"INFO","content":{"comment": "sold", "source": "sim", "extras": {"category": "sim", "action": "log_sale", "vendor": "vendor0", "consumer": "consumer1", "product": "oranges", "quantity": 6, "unit_price": 0.21, "total_price": 1.26, "extras": {}}}}
"""
    try:
        return obj['content']['comment']== "sold" and obj['content']['extras']['action']== "log_sale"
    except:
        return False
    
def get_sale_data(obj):
    try:
        return obj['content']['extras']
    except KeyError:
        return None
    

def report(logfile):

    with open(logfile) as f:
        lines = f.readlines()

    current_turn = 0

    sales = []
    
    for line in lines:
        try:
            data = json.loads(line)
        except json.decoder.JSONDecodeError:
            pass
        else:
            #print("{}".format(data['time']))
            if is_begin_turn(data):
                current_turn = get_turn(data)
                #print("{}".format(current_turn))
            elif is_sale(data):
                #print(get_sale_data(data))
                sales.append({**{"turn": current_turn}, **get_sale_data(data)})

    max_vendor = 0
    max_consumer = 0
    max_product = 0
    for sale in sales:
        max_vendor = max(max_vendor, len(sale['vendor']))
        max_consumer = max(max_consumer, len(sale['consumer']))
        max_product = max(max_product, len(sale['product']))
        
    for sale in sales:
        print("Turn {:3d}: {:>{width1}s} -> {:>{width2}s} £{} for {} {:>{width3}} [{}]".format(sale['turn'],
                                                                                 sale['vendor'],
                                                                                 sale['consumer'],
                                                                                 sale['total_price'],
                                                                                 sale['quantity'],
                                                                                               sale['product'],
                                                                                 '\u25CF' * sale['quantity'],
                                                                                 width1 = max_vendor,
                                                                                     width2 = max_consumer,
                                                                                     width3 = max_product))
        

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Report on a vendor-consumer simulation')

    parser.add_argument('--log', dest='log', type=str,
                    help='simulation log file')
    
    args = parser.parse_args()

    report(args.log)
    
