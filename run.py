#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""run.py
   
   runs a vendor-consumer simulation 
   
"""

import argparse
import asyncio
import concurrent.futures
import json
import logging #need the constants for logging level
import multiprocessing
import multiprocessing_logging
import time
from .utilities import config, heartbeat, logger, msg, spawn
import warnings
import zmq
import zmq.asyncio

""" suppress runtime warnings about unawaited coroutines
that are due to running them in different processs """
warnings.filterwarnings("ignore", message="coroutine")


async def run_tasks(tasks):

    log = logger.get_logger(__name__, logging_level = logging.INFO)

    loop = asyncio.get_event_loop()

    try:
        done, pending = await asyncio.wait(tasks, timeout=None, return_when=asyncio.FIRST_COMPLETED)
    except asyncio.CancelledError:
        
        for task in tasks:
            task.cancel()
            
            try:
                await asyncio.wait(task)
            except asyncio.CancelledError:
                pass
  
    #except Exception as e:
    #    log.critical(msg.corelog(msg.ERROR, extras = { msg.ERROR: msg.safe(e)}))


def main(config):
    """ run the simulation 

        Note that due to a possible issue with pyzmq, 
        pub-sub does not work as specified when there is 
        more than one sub in the same event loop if the pub 
        is also present in the same event loop (#issue 1208)
        This is particularly piquant for vendor consumers
        that have to talk to each other ....

    """
    log = logger.get_logger(__name__, logging_level = logging.INFO)

    beater_queue = multiprocessing.Queue()
    stop_event = multiprocessing.Event()

    heartbeat_task = [heartbeat.monitor(config, beater_queue, stop_event)]
      
    clock_tasks = spawn.agents_of_type("clocks", config, beater_queue, stop_event)
    consumer_tasks = spawn.agents_of_type("consumers", config, beater_queue, stop_event)
    vendor_tasks = spawn.agents_of_type("vendors", config, beater_queue, stop_event)
    vendorconsumer_tasks = spawn.agents_of_type("vendorconsumers", config, beater_queue, stop_event)
    
    # Assign task lists to a process per list 
    task_list = [heartbeat_task, consumer_tasks, vendor_tasks, clock_tasks]
    
    # Asign each vendorconsumer task to its own process
    if True:
        vendorconsumer_count  = 0
        for vendorconsumer_task in vendorconsumer_tasks:
            task_list.append([vendorconsumer_task,
                              heartbeat.beater(config,
                                               beater_queue,
                                               "vendorconsumer{}".format(vendorconsumer_count),
                                               stop_event)])
            vendorconsumer_count = vendorconsumer_count + 1
    else: 
        log.warning(msg.corelog("run.py is not launching vendorconsumer tasks"))
    
    processes = [] #we'll hold running process references here
    
    for task in task_list:
        processes.append(multiprocessing.Process(target=asyncio.run, args=(run_tasks(task),)))
        
    for process in processes:
        process.start()

    

    for process in processes:
        process.join()

    # simulation ends here when all processes join (return)    
    
if __name__ == "__main__":

    #logging to console for debugging
    log = logger.get_logger(__name__, logging_level = logging.INFO)
    log.info(msg.corelog("===============================STARTING======================================"))
    multiprocessing_logging.install_mp_handler()

    parser = argparse.ArgumentParser(description='Runs a vendor-consumer simulation')

    parser.add_argument('--config', dest='config', type=str,
                    help='simulation configuration file')
    
    args = parser.parse_args()
    log.debug(msg.corelog("command line args: {}".format(args)))

    configdata = config.reader(args.config)
    
    log.info(msg.corelog(f"started at {time.strftime('%X')}"))
    
    #asyncio.run(main(configdata, args.group))

    main(configdata)

    
    log.info(msg.corelog(f"finished at {time.strftime('%X')}"))
 

    
    
