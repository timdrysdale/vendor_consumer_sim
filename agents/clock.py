#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""clock.py
   
   implements clock worker for the simulator 
   
"""
import asyncio
import json
import logging 
from ..utilities import logger, msg
import zmq
import zmq.asyncio


def get_coro(coro):
    coros = {
        "debug": debug,
        "basic": basic
    }
    return coros[coro]

async def debug(config):
    print(config)
    return

# State machine states
REGISTERING = u'registering'
COUNTING = u'counting'
ANNOUNCING = u'announcing'
RUNNING = u'running'
ENDING_AGENTS = u'ending_agents'
ENDING_JOURNALS = u'ending_journals'

sync_states = [REGISTERING, COUNTING, ANNOUNCING]
turn_states = [REGISTERING, RUNNING, ENDING_AGENTS, ENDING_JOURNALS]


async def sync(config, queues, stop):
    try:
        log = logger.get_logger(__name__, logging_level = logging.INFO)
    
        # Count subscribers so we can check everyone has subscribed
        subscriber_categories = ['consumers','vendors','vendorconsumers']
        subscriber_count = 0
        for subscriber_category in subscriber_categories:
            subscriber_count = subscriber_count + len(config['others']['list_of'][subscriber_category])
            
        
        state = REGISTERING
        finished_count = 0
        current_turn = 0
        queue_sync = queues['sync']  
        queue_publish = queues['publish']
    
        while stop.is_set() == False:
            await asyncio.sleep(0)
            
            if state == REGISTERING:
                if finished_count < subscriber_count:
    
                    try:
                        message = queue_sync.get_nowait() #no await for _nowait
                        log.debug(msg.corelog(msg.RECEIVED, extras = {msg.MESSAGE: message}))
                        
                    except json.decoder.JSONDecodeError as e:
                        log.warning(msg.corelog(msg.JSONDecodeError,
                                                extras = {msg.MESSAGE: msg.safe(message),
                                                          msg.ERROR: e}))
    
                    except asyncio.QueueEmpty:
                        pass
                    
                    else:
                        
                        if msg.is_alert_that_registered_for_clock(message):
                            finished_count = finished_count + 1
                            log.debug(msg.corelog(msg.SUBSCRIBER_ADDED,
                                                  extras = {msg.REGISTERED: finished_count,
                                                            msg.EXPECTED: subscriber_count}))
                            
                        else:
                            log.warning(msg.corelog(msg.UNEXPECTED_MESSAGE,
                                                    extras = {msg.MESSAGE: message,
                                                              msg.EXPECTED: msg.REGISTERED_FOR_CLOCK}))          
                            
                else: #all subscribers are now registered
                    
                    log.info(msg.corelog(msg.ALL_REGISTERED_FOR_CLOCK))
                    await queue_publish.put(msg.begin_turn(current_turn))
                    log.info(msg.simlog(msg.BEGIN_TURN, extras = {msg.TURN: current_turn})) 
                    finished_count = 0                    
                    state = COUNTING
                    
            elif state == COUNTING: #how many subscribers have finished their turn
                if finished_count < subscriber_count:
                    try:
                        message = queue_sync.get_nowait()  #no await for _nowait
                        log.debug(msg.corelog(msg.RECEIVED, extras = {msg.MESSAGE: message}))
                        
                    except json.decoder.JSONDecodeError as e:
                        log.warning(msg.corelog(msg.JSONDecodeError, extras = {msg.MESSAGE: msg.safe(message), msg.ERROR: e}))
    
                    except asyncio.QueueEmpty:
                        pass
                    
                    else:
                        if msg.is_alert_that_finished_turn(message):
                            finished_count = finished_count + 1
                            log.debug(msg.corelog(msg.AGENT_FINISHED_TURN,
                                                  extras = {msg.FINISHED_COUNT: finished_count,
                                                            msg.EXPECTED: subscriber_count,
                                                            msg.TURN: message[msg.EXTRAS][msg.TURN],
                                                            msg.NAME: message[msg.EXTRAS][msg.NAME]}))
                            
                        else:
                            log.warning(msg.corelog(msg.UNEXPECTED_MESSAGE,
                                                    extras = {msg.MESSAGE: message,
                                                              msg.EXPECTED: msg.FINISHED_TURN}))                     
                else:
                    state = ANNOUNCING
    
            elif state == ANNOUNCING:
                log.debug(msg.corelog(msg.ALL_AGENTS_FINISHED_TURN, extras = {msg.TURN: current_turn}))
                current_turn  = current_turn + 1
                
                if current_turn > config['turns']:
                    stop.set()
                    break
                else:
                    finished_count = 0
                    state = COUNTING
                    await asyncio.sleep(0.1) #avoid REQREP getting out of sync at turn boundaries
                    await queue_publish.put(msg.begin_turn(current_turn))
                    log.info(msg.simlog(msg.BEGIN_TURN, extras = {msg.TURN: current_turn}))
                
    except Exception as e:
        log.critical(msg.corelog("Error", extras = {msg.ERROR: msg.safe(e)}))
    
async def basic(config):
    """The main job of the clock is to synchronise the actions of the agents, 
    and stop them at the end of the simulation. An multiprocessing event is shared
    amongst all processes to achieve the stop, while pyzmq publish is used to distribute
    new turn annoucements, while clients contacting the clock over REPREQ does the turn 
    synchronisation
    """
    try:
        #logging to console for debugging
        log = logger.get_logger(__name__, logging_level = logging.INFO)
    
        global_stop = config['stop_event']
        
        ctx = zmq.asyncio.Context()
        poller = zmq.asyncio.Poller()
    
        queue_publish = asyncio.Queue() #for publishing messages to subscribers
        queue_sync = asyncio.Queue() #for sending incoming REQ on sync socket to sync task
        queue_turns = asyncio.Queue() #for sending messages from sync to turns
        
        
        queues = {
            "publish": queue_publish,
            "sync":  queue_sync,       
            "turns": queue_turns
        }
        
        log.debug(msg.corelog("creating clock publication PUB socket{}".format(config['my_socket'])))
        clock_socket = ctx.socket(zmq.PUB)
        clock_socket.bind(config['my_socket'])
    
        log.debug(msg.corelog("creating clock synchronisation REP socket {}".format(config['sync_socket'])))
        sync_socket = ctx.socket(zmq.REP)
        sync_socket.bind(config['sync_socket'])
        poller.register(sync_socket, zmq.POLLIN)
    
        sync_task = asyncio.create_task(sync(config, queues, global_stop)) #no await for create_task
        log.debug(msg.corelog("started sync task"))
                                        
        while global_stop.is_set() == False:
            
            #Receive
            polled_sockets = dict(await poller.poll(timeout = 0.1))
    
            #check for incoming requests
            if sync_socket in polled_sockets and polled_sockets[sync_socket] == zmq.POLLIN:
    
                message = await asyncio.wait_for(sync_socket.recv_json(), timeout = 0.1)
                log.debug(msg.corelog(msg.RECEIVED, extras = {msg.MESSAGE: message}))
                
                #auto-acknowledge
                await asyncio.wait_for(sync_socket.send_json(msg.acknowledge(message)), timeout = 0.1)
                                        
                try:
                    await queue_sync.put(message)
                    
                except json.decoder.JSONDecodeError as e:
                    log.warning(msg.corelog(msg.JSONDecodeError, extras = {msg.MESSAGE: msg.safe(message), msg.ERROR: e}))
            #SEND
            
            #check for replies to send
            while True: #we'll break out
                await asyncio.sleep(0)
                try:
                    message = queue_publish.get_nowait()
                except json.decoder.JSONDecodeError as e:
                    log.warning(msg.corelog(msg.JSONDecodeError, extras = {msg.MESSAGE: msg.safe(message), msg.ERROR: e}))
                   
                except asyncio.QueueEmpty:
                    break #nothing to send just now, break out of loop
                else:
                    log.debug(msg.corelog(msg.PUBLISHING, extras = {msg.MESSAGE: message}))
                    await clock_socket.send_multipart([b'time',json.dumps(message).encode('utf-8')])
                        
            
        #await turns_task
        await sync_task
        await asyncio.sleep(0.1) #small pause to ensure last messages are sent
        clock_socket.close()
        sync_socket.close()
        
        log.debug(msg.corelog(msg.SOCKETS_CLOSED))
        
        
        return
    
    except Exception as e:
        log.critical(msg.corelog("Error", extras = {msg.ERROR: msg.safe(e)}))  
