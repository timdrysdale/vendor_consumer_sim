#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""agent.py
   
   implements generic agent for the simulator 
   requires a policy coroutine to do anything though 
   
"""
import asyncio
import copy
import json
import logging #need the constants for logging level
from ..policies import demo
import time
from ..utilities import logger, msg

import zmq
import zmq.asyncio

CONSUMER = u'consumer'
VENDORCONSUMER = u'vendorconsumer'
VENDOR = u'vendor'

def get_roles(config, name):
    agent_type = config['details_of']['name']['is']
    if agent_type == VENDORCONSUMER:
        return [CONSUMER, VENDOR]
    else:
        return [agent_type]


def get_policy_lists():
    return{
        "demo": demo.get_policies()
    }

def get_coro(coro):
    coros = {
        "debug": debug,
        "basic": basic
    }
    return coros[coro]


async def debug(config):
    print(config)
    return

async def policy_demo(config, queues_dict, stop_event):
    my_name = config['name']
    log = logger.get_logger(my_name, logging_level = logging.INFO)
    log.debug(queues_dict)
    while stop_event.is_set() == False:
        await asyncio.sleep(0.3)
        message = {"to":"vendor0","content":{"message":"BUY!"}}
        await queues_dict['out']['send_request'].put(json.dumps(message))
        log.info({"time":time.time(),"message":message})#use sim time here not real time
    return



async def basic(config):
    try: 
        my_name = config['name']
    
        my_type = config['my_type']
        
        log = logger.get_logger(my_name, logging_level = logging.INFO)
    
        ctx = zmq.asyncio.Context.instance()
    
        global_stop = config['stop_event']

        if len(config['others']['list_of']['clocks']) > 1:
            log.warning("Currently only syncing with first clock")
        
        #TODO get this from config, and find in module according to agent type
        #log.warning("Using demo policy and ignoring config")
        #policy_coro = policy_demo #config['policy_coro']
        #policy_config = config
    
        available_policies = get_policy_lists()
    
        policy_classname = config['policy']['class']
        policy_coroname = config['policy']['coro']
        
        try:
            policy_coro = available_policies[policy_classname][policy_coroname]
            policy_config = config['policy'] #['parameters']
            policy_config['name'] = config['name']
        except KeyError:
            log.critical("Can't find policy {}.{}".format())
            global_stop.set()
            return
    
        list_of = config['others']['list_of']
    
        contacts_sockets = [] #list to help with polling
        contacts = {} #details to help with sending REQ to contacts
        
        if my_type == u'vendors':
            list_of_contacts = list_of['consumers'] + list_of['vendorconsumers']
            policy_config['list_of_consumers'] = list_of_contacts 
            policy_config['list_of_contacts'] = list_of_contacts  
            policy_config['list_of_vendors'] = []
            
        elif my_type == u'consumers':
            list_of_contacts = list_of['vendors'] + list_of['vendorconsumers']
            policy_config['list_of_consumers'] = []
            policy_config['list_of_contacts'] = list_of_contacts
            policy_config['list_of_vendors'] = list_of_contacts
            
        elif my_type == u'vendorconsumers':
            #remove self from the list
            list_of_consumervendors = copy.deepcopy(list_of['vendorconsumers'])
            list_of_consumervendors.remove(my_name)
            list_of_contacts = list_of['consumers'] + list_of['vendors'] + list_of_consumervendors
            policy_config['list_of_consumers'] = list_of['consumers'] + list_of_consumervendors
            policy_config['list_of_contacts'] = list_of_contacts 
            policy_config['list_of_vendors'] = list_of['vendors'] + list_of_consumervendors
            
        else:    
            log.critical("I have unknown agent type {}".format(my_name, my_type))
            stop.set()
        
        # Initialize poll set
        poller = zmq.asyncio.Poller()
    
        # open sockets to make requests to every contact in our contact list
        for contact_name in list_of_contacts:
            contact_url = config['others']['details_of'][contact_name]['url']
            contact_socket = ctx.socket(zmq.REQ)
            contact_socket.connect(contact_url)  
            contacts[contact_name] = {"name":contact_name, "socket":contact_socket}
            contacts_sockets.append(contact_socket)
            poller.register(contact_socket, zmq.POLLIN)
            log.debug("Connected to contact {} at socket:{}".format(contact_name, contact_socket))        
        
        # open a socket to receive requests from contacts
        my_socket = ctx.socket(zmq.REP)
        my_socket.bind(config['my_socket'])
        poller.register(my_socket, zmq.POLLIN)
        log.debug("Bound to my socket at :{}".format(config['my_socket']))
        
         
        # open sockets to receive clock messages
        clock_sockets = [] #list to help with polling
        sync_sockets = [] #list to help with registering with clocks
        
        list_of_clocks = config['others']['list_of']['clocks']
            
        for clock_name in list_of_clocks:
            clock_url = config['others']['details_of'][clock_name]['url']
            clock_socket = ctx.socket(zmq.SUB)
            clock_socket.connect(clock_url)  
            clock_sockets.append(clock_socket)
            list_of_topics = config['others']['details_of'][clock_name]['list_of_topics']
             
            for topic in list_of_topics:
                clock_socket.setsockopt(zmq.SUBSCRIBE, topic.encode('utf-8'))
                
            poller.register(clock_socket, zmq.POLLIN)
            log.debug("Subscribed to clock {} at {} for topics {}".format(clock_name, clock_url, ", ".join(list_of_topics)))
    
            sync_socket = ctx.socket(zmq.REQ)
            sync_url = config['others']['details_of'][clock_name]['url_sync']
            sync_socket.connect(sync_url)
            sync_sockets.append(sync_socket)
            poller.register(sync_socket, zmq.POLLIN)
            log.debug("Connected to clock sync at {}".format(sync_url))
        
        # message handling here
    
        queue_in_clock = asyncio.Queue()
        queue_in_receive_request = asyncio.Queue()  # someone making a request of us
        queue_in_receive_reply = asyncio.Queue()    # a reply we got to a request we made of someone
        queue_out_clock = asyncio.Queue()   # for finishing a turn
        queue_out_send_reply = asyncio.Queue()   # reply to a request that someone made to us
        queue_out_send_request = asyncio.Queue() # us making a request to someone
    
        
        #stop = asyncio.Event()
        
        queues = {
            "in":{
                "clock":   queue_in_clock,       
                "receive_request": queue_in_receive_request, 
                "receive_reply":   queue_in_receive_reply
            },
            "out":{
                "clock":   queue_out_clock,
                "send_reply":   queue_out_send_reply,  
                "send_request": queue_out_send_request
            }
        }

        try:

            policy_task = asyncio.create_task(policy_coro(policy_config, queues, global_stop)) #no await for create_task
            
        except asyncio.CancelledError:
            raise
        
        await asyncio.sleep(1.0)
        #register with the clocks
        
        #for sync_socket in sync_sockets:
        ss = sync_socket    
    
        log.debug("registering for clock sync {}".format(ss))
    
        registration_message = msg.registered_for_clock(my_name)
        
        await ss.send_json(registration_message) 
        
        log.debug(registration_message)
    
        while global_stop.is_set() == False: #break when acknowledged
    
            await asyncio.sleep(0)
            
            try:
                message = await asyncio.wait_for(ss.recv_json(), timeout = 0.5)
            except asyncio.TimeoutError:
                pass
            else:
                log.debug("Registration acknowledged {}".format(message))
                break 
    
            
        while global_stop.is_set() == False:
            try:
                message = await asyncio.wait_for(ss.recv_json(), timeout = 0.01)
            except asyncio.TimeoutError:
                pass
            else:
                log.debug("Turn finished acknowledged {}".format(message))
                
            
            #Receive
            polled_sockets = dict(await poller.poll(timeout = 0.01))
    
            #check for incoming clock messages 
            for socket in clock_sockets:
                if socket in polled_sockets and polled_sockets[socket] == zmq.POLLIN:
                    [topic, message] = await asyncio.wait_for(socket.recv_multipart(), timeout = 0.01)
                    log.debug("clock published {}".format(message))
                    await queue_in_clock.put(message)
    
            
    
    
            #check for incoming replies 
            for socket in contacts_sockets:
                if socket in polled_sockets and polled_sockets[socket] == zmq.POLLIN:
                    message = await asyncio.wait_for(socket.recv_json(), timeout = 0.01)
                    #leave info logging to the policy - it knows what matters
                    log.debug(message)
                    try:
                        await queue_in_receive_reply.put(message)
                    except asyncio.QueueFull:
                        log.critical(msg.corelog("QueueFull!"))
                        global_stop.set()
                        
            
            #check for incoming requests
            if my_socket in polled_sockets and polled_sockets[my_socket] == zmq.POLLIN:
                message = await asyncio.wait_for(my_socket.recv_json(), timeout = 0.01)
                log.debug(message) #leave info logging to the policy - it knows what matters
                try:
                    queue_in_receive_request.put_nowait(message)
                except asyncio.QueueFull:
                    log.critical(msg.corelog("QueueFull!"))
                    global_stop.set()
                
            #SEND
            
            #check for replies to send
            while True: #we'll break out
                await asyncio.sleep(0)
                try:
                    message = queue_out_send_reply.get_nowait()
    
                except json.decoder.JSONDecodeError:
                    log.warning(msg.simlog("JSONDecodeError from reply generated by policy {}".format(message)))
                except asyncio.QueueEmpty:
                    break #nothing to send just now, break out of loop
                else:
                    log.debug(msg.corelog("agent_core got a  reply to send {}".format(message)))
                    if msg.is_send_command(message):
                        
                        await my_socket.send_json(msg.content(message))
                    elif msg.is_send_to_command(message):
                        log.warning(msg.corelog("This socket cannot 'send_to', only 'send', so stripping address", extras = {msg.MESSAGE: message}))
                        await my_socket.send_json(msg.content(message))
                    else:
                        log.warning(msg.corelog(msg.UNEXPECTED_MESSAGE, extras = {msg.MESSAGE: message}))
            
            #check for requests to send
            while True: #we'll break out
                await asyncio.sleep(0)
                try:
                    request = queue_out_send_request.get_nowait()
    
                except json.decoder.JSONDecodeError:
                    log.warning(msg.simlog("JSONDecodeError from reply generated by policy {}".format(message)))
                except asyncio.QueueEmpty:
                    break #nothing to send just now, break out of loop
                else:
                    try:
                        if msg.is_send_to_command(request):
                            await contacts[msg.recipient(request)]['socket'].send_json(msg.content(request))
                    except KeyError:
                        log.warning("Policy wants to make request to unknown recipient {}".format(request))
    
            
            #check for messages to the clock
            while True: #we'll break out
                await asyncio.sleep(0)
                try:
                    message = queue_out_clock.get_nowait()
    
                except json.decoder.JSONDecodeError:
                    log.warning(msg.simlog("JSONDecodeError from clock message generated by policy {}".format(message)))
                except asyncio.QueueEmpty:
                    break #nothing to send just now, break out of loop
                else:
                    if msg.is_send_command(message):
                        await ss.send_json(msg.content(message))
                    elif msg.is_send_to_command(message):
                        log.warning(msg.corelog("This socket cannot 'send_to', only 'send', so stripping address", extras = {msg.MESSAGE: message}))
                        await ss.send_json(msg.content(message))
                    else:
                        log.warning(msg.corelog(msg.UNEXPECTED_MESSAGE, extras = {msg.MESSAGE: message}))
           
    
            
        """ shut down sockets and policy handler """
    
        for socket in clock_sockets + sync_sockets + contacts_sockets:
            await asyncio.sleep(0)
            socket.close() #no await for a socket close
    
        log.debug(msg.corelog(msg.SOCKETS_CLOSED))

        try:

            await policy_task #wait for policy task to stop

        except asyncio.CancelledError:

            policy_task.cancel()

            try:

                await policy_task
                log.debug(msg.corelog(msg.POLICY_TASK_ENDED))
        
            except asyncio.CancelledError:

                pass
            
        
        return

    except asyncio.CancelledError:
        pass
    


