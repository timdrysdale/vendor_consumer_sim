#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""demo.py
   
   implements simple policies for a demonstration of the simulator
   
"""

import asyncio
import copy
import json
import logging #need the constants for logging level
import re
import time
from ..utilities import logger, msg

"""

Example policy construction

async def markup_active(config, queues_dict, stop_event, state, log):
    log.debug("policy markup active")
    return state

async def markup_passive(config, queues_dict, stop_event, state, log):
    log.debug("policy markup passive")
    return state


async def markup(config, queues_dict, stop_event):
   active_phase = markup_active
   passive_phase = markup_passive
   await base_policy(config, queues_dict, stop_event, active_phase, passive_phase)

"""

ACTIVE = u'active'
PASSIVE = u'passive'
WAITING = u'waiting'

states = [ACTIVE, PASSIVE] 

async def base_policy(config, queues_dict, stop_event, active_phase, passive_phase):

    my_name = config['name']
    log = logger.get_logger(my_name, logging_level = logging.DEBUG)
    current_turn = 0
    policy_state = {}

    next_state = WAITING

    queue_clock_in = queues_dict['in']['clock']
    queue_clock_out = queues_dict['out']['clock']
    
    while stop_event.is_set() == False:
   
        if next_state == WAITING:
            log.debug("base_policy waiting phase, turn {}".format(current_turn))
            # TODO: change to use json from the clock for consistency
            message = await queue_clock_in.get() #no need for timeout, nothing else to do
            log.debug(msg.corelog("base_policy turn started? {}".format(message)))
            try:
                message = json.loads(message.decode('utf-8'))
            except json.decoder.JSONDecodeError:
                log.warning(msg.corelog(msg.JSONDecodeError, extras={msg.MESSAGE: message}))
            else:
                log.debug(msg.corelog("base_policy turn started? {}".format(message)))
        
                if msg.is_begin_turn_command(message):
                    turn_number = msg.turn_number(message)
                    if turn_number >= current_turn:
                        log.debug(msg.corelog("turn {} started".format(current_turn, message)))
                        next_state = ACTIVE
                else:
                    log.debug(msg.corelog("base_policy not a BEGIN_TURN command"))
                   
        elif next_state == ACTIVE:
            log.debug(msg.corelog("base_policy policy active phase, turn {}".format(current_turn)))
            policy_state = await active_phase(copy.deepcopy(config), queues_dict, stop_event, policy_state, log)
            await queue_clock_out.put(msg.send_message(msg.finished_turn(my_name, current_turn)))
            next_state = PASSIVE
               
        else:    
            #PASSIVE
            log.debug(msg.corelog("base_policy passive phase, turn {}".format(current_turn)))
            policy_state = await passive_phase(copy.deepcopy(config), queues_dict, stop_event, policy_state, log)
            next_state = WAITING
            current_turn = current_turn + 1
            
    return



    
