#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""demo.py
   
   implements simple policies for a demonstration of the simulator
   
"""
from ..agents import agent
import asyncio
import copy
import json
import logging #need the constants for logging level
import random
import re
import time
import traceback
from ..utilities import logger, msg
from .base import base_policy

def get_policies():
    return {
        "buy_below_price": buy_below_price,
        "markup": markup,
        "sell_bulk": sell_bulk
    }


#https://stackoverflow.com/questions/58def decision(probability):return random.random() < probability86987/true-or-false-output-based-on-a-probability
def decision(probability):
    return random.random() < probability

# MARKUP



async def markup_active(config, queues_dict, stop_event, state, log):
    log.debug("policy markup active")
    #send selling message to all customers, once have bought something
    return state

async def markup_passive(config, queues_dict, stop_event, state, log):
    log.debug("policy markup passive")
    #buy something to on-sell
    time.sleep(0.1)
    return state


async def markup(config, queues_dict, stop_event):
   active_phase = sell_bulk_active
   passive_phase = buy_below_price_passive
   await base_policy(config, queues_dict, stop_event, active_phase, passive_phase)
   return

# sell_bulk
"""
"parameters":{
		     "products": [
			 {"oranges":{
			     "price":0.36,
			     "moq": 12,
			  "name":"oranges",
			  "description":"naval",
			  "category": "fruit",
			  "quantity": 24
			 }
			 }
			 
		     ]
"""


MAKE_OFFER = u'make_offer'
AWAIT_REPLY = u'await_reply'

states = [MAKE_OFFER, AWAIT_REPLY]

async def sell_bulk_active(config, queues_dict, stop_event, state_data, log):
    """ This is a two-state state machine that takes a single pass through one state 
    each time it runs. state data is persisted via state_data
    """
    log.debug("policy sell_bulk active")
    log.debug('policy config {}'.format(config))
    name = 'sell_bulk_active'
    my_name = config['name']
    product_done = []
    contact_done = []
    
    log.debug(msg.simlog("{} starting active with state {}".format(name, state_data)))

    # reload our current state, or do initial preparation of the state data
    if ('current_state' in state_data and 'list_of_consumers' in state_data and 'products' in state_data):
        contacts = state_data['list_of_consumers']
        products = state_data['products']
        current_state = state_data['current_state']
    else:
        # initial preparation of the state data
        log.debug(msg.simlog('configuring policy sell_bulk')) 
        contacts = copy.deepcopy(config['list_of_consumers'])
        state_data['list_of_consumers'] = contacts
        

        try:
            products = copy.deepcopy(config['parameters']['products'])
        except KeyError:
            products = []

        state_data['products'] = products    
        current_state = MAKE_OFFER

    #-------------------MAKE_OFFER----------------------------------#    

    if current_state == MAKE_OFFER:
        await asyncio.sleep(0)
        valid_offer = False
        offer_attempts_left = 50
        while not valid_offer:
            try:
                offer_product = random.randint(1,len(products))-1
                offer_contact = random.randint(1,len(contacts))-1
                #check
                log.info(msg.simlog("Policy sell_bulk offer for {} to {}".format(products[offer_product]['name'], contacts[offer_contact])))
            except (ValueError, IndexError) as e:
                #seems we've nothing to sell or noone to sell to just now
                #try again next turn ....
                return state_data

            # do we have enough to sell?
            available = products[offer_product]['quantity']
            moq = products[offer_product]['moq']
            if available >= moq: 
                valid_offer = True
                name = products[offer_product]['name']
                price = products[offer_product]['price']
                contact = contacts[offer_contact]
            else:
                offer_attempts_left = offer_attempts_left -1
                if offer_attempts_left <= 0:
                    #we've not found an offer so let the turns move on without us making an offer
                    state_data['current_state'] = MAKE_OFFER #next state
                    return state_data
                
                
        offer = msg.offer_to_sell(msg.offer_details(name, price, my_name, contact, quantity= {"min":moq,"max":available}))
    
        message = msg.send_message(offer, to = contact)
        
        try:
            queues_dict['out']['send_request'].put_nowait(message)
            
        except asyncio.QueueFull:
            #never mind, make a different offer next time
            state_data['current_state'] = MAKE_OFFER #next state
            return state_data
            
        else:
            log.info(msg.corelog("Policy sell_bulk sent offer {}", offer))
            state_data['current_offer'] = offer
            state_data['current_state'] = AWAIT_REPLY #next state
            return state_data

    #---------------------AWAIT_REPLY-----------------------------#
      
    elif current_state == AWAIT_REPLY:
        await asyncio.sleep(0)      
        try:
            log.info("policy sell_bulk waiting on reply {}")
            reply = await asyncio.wait_for(queues_dict['in']['receive_reply'].get(), timeout =0.01)
            
        except asyncio.TimeoutError:
            state_data['current_state'] = AWAIT_REPLY
            return state_data #exit to handler, finish our active turn
        
        else:
            log.info("policy sell_bulk got reply {}".format(reply))
            
            if msg.is_accepting_offer(reply):
                quantity = msg.get_quantity(reply)
                product = msg.get_product(reply)
                
                for item in state_data['products']:
                    if item['name'] == product:
                        item['quantity'] = item['quantity'] - quantity
                        
                log.info(msg.simlog(msg.SOLD, extras = msg.log_sale(state_data['current_offer']['offer'][msg.VENDOR],
                                                                    state_data['current_offer']['offer'][msg.CONSUMER],
                                                                    reply)))
                
            state_data['current_state'] = MAKE_OFFER       
            return state_data
        
        log.debug(msg.simlog("{} state {}".format(name, state)))
    else:
        #shouldn't get here
        state_data['current_state'] = MAKE_OFFER
        return state_data
    


async def sell_bulk_passive(config, queues_dict, stop_event, state, log):
    log.debug("policy sell_bulk passive")
    #no passive state
    return state


async def sell_bulk(config, queues_dict, stop_event):
   active_phase = sell_bulk_active
   passive_phase = sell_bulk_passive
   await base_policy(config, queues_dict, stop_event, active_phase, passive_phase)
   return

# buy_below_price

""" only buys if price is below certain threshold 

		 "coro":"buy_below_price",
		 "parameters":{
		     "desires":"oranges",
		     "price":{
			 "max":0.47
		     }, 
		     "quantity":{
			 "min":1,
			 "max":3
		     }
"""

async def buy_below_price_active(config, queues_dict, stop_event, state, log):
    log.debug("policy buy_below_price active bbp")
    #no active phase in this policy
    return state

async def buy_below_price_passive(config, queues_dict, stop_event, state, log):
    
        log.debug("policy buy_below_price passive bbp")
        #buy if meets threshold
        while stop_event.is_set()==False:
            await asyncio.sleep(0)
        
            try:
                log.info(msg.corelog("policy buy_below_price awaiting message"))
                message = queues_dict['in']['receive_request'].get_nowait()
                
                log.info(msg.corelog("policy buy_below_price received request of type {}".format(type(message),extras={msg.MESSAGE: message})))
                
            except asyncio.QueueEmpty:
                log.info(msg.corelog("policy buy_below_price no message"))
                break
            else:
                log.info(msg.corelog("policy buy_below_price trying to reply to offer"))
                try:

                    accept_offer = True
                    
                    offer = msg.get_offer(message)
                    price = offer[msg.PRICE]
                    quantity = offer[msg.QUANTITY]
                    try:
                        max_price = config['parameters']['price']['max']
                        if price > max_price:
                            accept_offer = False
                    except KeyError:
                        pass
                        
                    if isinstance(quantity,int):
                        sell_min = quantity
                        sell_max = quantity
                    else:
                        try:
                            sell_min = quantity[msg.MIN]
                            sell_max = quantity[msg.MAX]
                        except KeyError:
                            log.critical(msg.simlog('{} unknown quantity {} of type {}'.format(msg.UNEXPECTED_MESSAGE, quantity, type(quantity))))
                            stop_event.set()

                    try:        
                        desire_min = config['parameters']['quantity']['min']
                        desire_max = config['parameters']['quantity']['max']
                    except KeyError:
                        log.critical(msg.simlog('Config: unknown buy quantity {} of type {}'.format(config['parameters']['quantity'],
                                                                                                    type(config['parameters']['quantity']))))
                        stop_event.set()
                        
                    buy_min = max(sell_min, desire_min)
                    buy_max = min(sell_max, desire_max)
                    log.debug(msg.simlog('policy buy_below_price ranges: buy({}-{}) desire({}-{}) available({}-{})'.format(buy_min, buy_max, desire_min, desire_max, sell_min, sell_max)))
                    try:
                        buy_actual = random.randint(buy_min, buy_max)
                    except ValueError: #occurs buy_range outside sale range
                        accept_offer = False

                    
                    try:
                        final_decision_is_accept = decision(config['parameters']['probability']) and accept_offer
                    except KeyError:
                        pass

                    if final_decision_is_accept:
                        reply = msg.send_message(msg.accept_offer(msg.get_offer(message),buy_actual))
                    else:
                        reply = msg.send_message(msg.decline_offer(msg.get_offer(message)))
                        
                    log.debug(msg.corelog('policy buy_below_price reply: {}'.format(reply)))

                    await asyncio.wait_for(queues_dict['out']['send_reply'].put(reply), timeout = 0.01)
                    
                except asyncio.TimeoutError:
                    log.critical(msg.corelog("policy buy_below_price TimeoutError while trying to reply to offer"))
                    stop.is_set()
                else:
                    log.info(msg.corelog("policy buy_below_price sent reply to offer",
                                         extras={msg.MESSAGE: reply}))
                
        log.debug("policy buy_below_price finished passive bbp")   
        return state
                

        

async def buy_below_price(config, queues_dict, stop_event):
   active_phase =  buy_below_price_active
   passive_phase =  buy_below_price_passive
   await base_policy(config, queues_dict, stop_event, active_phase, passive_phase)
   return

  
